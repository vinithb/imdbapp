package com.vinith.imdbapp.feature.searchTitle.model.entity

data class IX(
    val height: Int,
    val imageUrl: String,
    val width: Int
)