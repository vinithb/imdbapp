package com.vinith.imdbapp.feature.searchTitle.model.entity

import com.vinith.imdbapp.feature.model.entity.V

data class D(
    val i: I,
    val id: String,
    val l: String,
    val q: String,
    val rank: Int,
    val s: String,
    val v: List<V>,
    val vt: Int,
    val y: Int,
    val yr: String
) {
//        fun toMovie() = Movie(l, s, i?.imageUrl)
}