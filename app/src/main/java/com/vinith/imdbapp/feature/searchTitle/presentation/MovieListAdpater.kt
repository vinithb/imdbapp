package com.vinith.imdbapp.feature.searchTitle.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vinith.imdbapp.R
import com.vinith.imdbapp.core.extension.loadImage
import kotlinx.android.synthetic.main.layout_movie_title.view.*
import java.util.ArrayList

class MovieListAdpater(private var movieList: java.util.ArrayList<MovieListView>?) :
    RecyclerView.Adapter<MovieListAdpater.MovieTitleHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieTitleHolder {
        val inflatedView = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_movie_title, parent, false)
        return MovieTitleHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: MovieTitleHolder, position: Int) {
        movieList?.get(position)?.let { holder.bindMovieTitleHolder(it) }
    }

    override fun getItemCount(): Int {
        if(movieList != null)
        return movieList?.size!!

        return 0
    }

    fun setMovieList(newMovieList: ArrayList<MovieListView>?) {
        movieList = newMovieList
    }

    class MovieTitleHolder(v: View) :
        RecyclerView.ViewHolder(v) {

        private var view: View = v

        fun bindMovieTitleHolder(title: MovieListView) {
            view.titleTV.text = title.title
            view.subTitleTV.text = title.actors
            title.image?.let { view.imageView.loadImage(it) }
        }
    }
}