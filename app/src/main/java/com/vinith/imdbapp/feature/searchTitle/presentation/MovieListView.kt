package com.vinith.imdbapp.feature.searchTitle.presentation

data class MovieListView(var title:String?, var actors:String?, var image:String?) {
    companion object {
        fun empty() = MovieListView("","","")
    }
}