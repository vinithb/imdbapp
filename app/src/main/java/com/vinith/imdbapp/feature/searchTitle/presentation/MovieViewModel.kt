package com.vinith.imdbapp.feature.searchTitle.presentation

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.vinith.imdbapp.core.exception.Failure
import com.vinith.imdbapp.core.platform.BaseViewModel
import com.vinith.imdbapp.feature.searchTitle.domain.SearchMovie
import com.vinith.imdbapp.feature.searchTitle.model.domain.Movie
import javax.inject.Inject

class MovieViewModel
@Inject constructor(private val searchMovie: SearchMovie) : BaseViewModel() {

    var movieViewList = MutableLiveData<ArrayList<MovieListView>>()

    fun searchFor(title: String) =
        searchMovie(SearchMovie.Params(getTitleText(title)), viewModelScope) {
            it.fold(
                ::handleFailure,
                ::handleMovieDetails
            )
        }

    private fun handleMovieDetails(list: Movie) {
        movieViewList.value = getMovieViewList(list)
    }

    private fun handleFailure(failure: Failure) {
        Log.d("Failure", failure.toString())
    }

    private fun getTitleText(title: String) =
        title.take(1) + "/" + title.replace(" ", "_") + ".json"

    private fun getMovieViewList(movie: Movie?): ArrayList<MovieListView> {
        val list = ArrayList<MovieListView>()
        if (movie != null) {
            for (movieDetail in movie.movieList!!) {
                val movieListView = MovieListView.empty()
                movieListView.title = movieDetail.title
                movieListView.actors = movieDetail.actors
                movieDetail.imageUrl?.let {
                    movieListView.image = it
                }
                list.add(movieListView)
            }
        }

        return list
    }
}