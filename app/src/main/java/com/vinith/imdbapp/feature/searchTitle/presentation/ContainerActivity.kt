package com.vinith.imdbapp.feature.searchTitle.presentation

import android.os.Bundle
import com.vinith.imdbapp.R
import com.vinith.imdbapp.core.extension.loadFragment
import com.vinith.imdbapp.core.extension.replaceFragment
import com.vinith.imdbapp.core.platform.BaseActivity

class ContainerActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)
        loadFragment(MovieSearchFragment.newInstance(), R.id.container)
    }

    fun loadNext() {
        replaceFragment(MovieDetailFragment.newInstance(), R.id.container)
    }
}