package com.vinith.imdbapp.feature.model.entity

import com.vinith.imdbapp.feature.searchTitle.model.entity.IX

data class V(
    val i: IX,
    val id: String,
    val l: String,
    val s: String
)