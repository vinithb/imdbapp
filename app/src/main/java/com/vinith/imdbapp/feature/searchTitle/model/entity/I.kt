package com.vinith.imdbapp.feature.searchTitle.model.entity

data class I(
    val height: Int,
    val imageUrl: String,
    val width: Int
)