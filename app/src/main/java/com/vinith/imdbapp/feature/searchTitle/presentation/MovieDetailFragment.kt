package com.vinith.imdbapp.feature.searchTitle.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.vinith.imdbapp.ImdbApp
import com.vinith.imdbapp.R
import com.vinith.imdbapp.core.di.viewmodel.ViewModelFactory
import com.vinith.imdbapp.core.platform.BaseFragment
import javax.inject.Inject

class MovieDetailFragment: BaseFragment(){

    companion object {
        fun newInstance() = MovieDetailFragment().apply {
            arguments = Bundle().apply { }
        }
    }

    private lateinit var viewModel: MovieViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.applicationContext as ImdbApp).appComponent.inject(this)

        viewModel = activity?.let {
            ViewModelProvider(it, viewModelFactory).get(MovieViewModel::class.java)
        }!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movie_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}