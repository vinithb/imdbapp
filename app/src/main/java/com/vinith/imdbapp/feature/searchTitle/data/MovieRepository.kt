package com.vinith.imdbapp.feature.searchTitle.data

import com.vinith.imdbapp.core.api.ImdbApi
import com.vinith.imdbapp.core.exception.Failure
import com.vinith.imdbapp.core.functional.Either
import com.vinith.imdbapp.core.platform.NetworkHandler
import com.vinith.imdbapp.feature.searchTitle.model.domain.Movie
import com.vinith.imdbapp.feature.searchTitle.model.entity.MovieEntity
import retrofit2.Call
import javax.inject.Inject

interface MovieRepository {

    fun search(title: String): Either<Failure, Movie>

    class Network
    @Inject constructor(private val networkHandler: NetworkHandler,
    private val service: ImdbApi) : MovieRepository {

        override fun search(title: String): Either<Failure, Movie> {
            return when (networkHandler.isNetworkAvailable()) {
                true -> request(
                    service.searchTitles(title),
                    { it.toMovie()},
                    MovieEntity.empty
                )

                false -> Either.Left(Failure.NetworkConnection)
            }
        }

        private fun <T, R> request(
            call: Call<T>,
            transform: (T) -> R,
            default: T
        ): Either<Failure, R> {
            return try {
                val response = call.execute()
                when (response.isSuccessful) {
                    true -> Either.Right(transform((response.body() ?: default)))
                    false -> Either.Left(Failure.ServerError)
                }
            } catch (exception: Throwable) {
                Either.Left(Failure.ServerError)
            }
        }
    }
}