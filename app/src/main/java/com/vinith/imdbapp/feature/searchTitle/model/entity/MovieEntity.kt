package com.vinith.imdbapp.feature.searchTitle.model.entity

import com.vinith.imdbapp.core.extension.empty
import com.vinith.imdbapp.feature.searchTitle.model.domain.Movie
import com.vinith.imdbapp.feature.searchTitle.model.domain.MovieDetails

data class MovieEntity(
    val d: List<D>,
    val q: String,
    val v: Int
) {
    companion object {
        val empty = MovieEntity(arrayListOf<D>(), String.empty(), 0)
    }

    fun toMovie(): Movie {
        val list = ArrayList<MovieDetails>()
        for (movie in d) {
            val movieDetails = MovieDetails.empty()
            movieDetails.title = movie.l
            movieDetails.actors = movie.s
            movie.i?.imageUrl?.let {
                movieDetails.imageUrl = it
            }
            list.add(movieDetails)
        }

        return Movie(list)
    }
}