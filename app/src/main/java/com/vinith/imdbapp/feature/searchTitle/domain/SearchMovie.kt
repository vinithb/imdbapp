package com.vinith.imdbapp.feature.searchTitle.domain

import com.vinith.imdbapp.core.interactor.UseCase
import com.vinith.imdbapp.feature.searchTitle.data.MovieRepository
import com.vinith.imdbapp.feature.searchTitle.model.domain.Movie
import javax.inject.Inject

class SearchMovie
@Inject constructor(private val movieRepo: MovieRepository) :
    UseCase<Movie, SearchMovie.Params>() {

    override suspend fun run(params: Params) = movieRepo.search(params.title)

    data class Params(val title: String)
}