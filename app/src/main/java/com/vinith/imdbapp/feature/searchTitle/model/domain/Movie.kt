package com.vinith.imdbapp.feature.searchTitle.model.domain

import com.vinith.imdbapp.core.extension.empty


data class Movie(var movieList: ArrayList<MovieDetails>?) {
    companion object {
        fun empty() = Movie(arrayListOf())
    }
}

data class MovieDetails(var title: String, var actors: String, var imageUrl: String) {
    companion object {
        fun empty() = MovieDetails(String.empty(), String.empty(), String.empty())
    }
}