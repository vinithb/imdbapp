package com.vinith.imdbapp.feature.searchTitle.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.vinith.imdbapp.ImdbApp
import com.vinith.imdbapp.R
import com.vinith.imdbapp.core.di.viewmodel.ViewModelFactory
import com.vinith.imdbapp.core.platform.BaseFragment
import kotlinx.android.synthetic.main.fragment_movie_search.*
import java.util.*
import javax.inject.Inject


class MovieSearchFragment: BaseFragment() {

    private lateinit var viewModel: MovieViewModel
    private var adapter: MovieListAdpater? = null

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    companion object {
        fun newInstance() = MovieSearchFragment().apply {
            arguments = Bundle().apply { }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.applicationContext as ImdbApp).appComponent.inject(this)

        viewModel = activity?.let {
            ViewModelProvider(it, viewModelFactory).get(MovieViewModel::class.java)
        }!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movie_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUI()
        attachObservers()
    }

    private fun attachObservers() {
        viewModel.movieViewList.observe(viewLifecycleOwner, {
            updateListAdapter(it)
        })
    }

    private fun updateListAdapter(list: ArrayList<MovieListView>?) {
        if (adapter == null) {
            adapter = MovieListAdpater(list)
            val linearLayoutManager = LinearLayoutManager(activity)
            rvItems.layoutManager = linearLayoutManager
            rvItems.adapter = adapter

            rvItems.addItemDecoration(DividerItemDecoration(
                rvItems.context,
                linearLayoutManager.orientation
            ))
        } else {
            adapter!!.setMovieList(list)
        }

        adapter?.notifyDataSetChanged()
    }

    private fun initUI() {
        searchView.onActionViewExpanded()
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if(newText.isNullOrEmpty()) {
                    updateListAdapter(null)
                } else {
                    viewModel.searchFor(newText)
                }
                return false
            }
        })
    }
}