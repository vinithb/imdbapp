package com.vinith.imdbapp

import android.app.Application
import com.vinith.imdbapp.core.di.AppComponent
import com.vinith.imdbapp.core.di.AppModule
import com.vinith.imdbapp.core.di.DaggerAppComponent

class ImdbApp : Application() {

    val appComponent: AppComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        DaggerAppComponent
            .builder().appModule(AppModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        this.injectMembers()
    }

    private fun injectMembers() = appComponent.inject(this)
}