package com.vinith.imdbapp.core.build

import com.vinith.imdbapp.BuildConfig

class BuildConstant {
    companion object {

        lateinit var buildConstantBase: BuildConstantBase

        init {
            when(BuildConfig.BUILD_TYPE) {
                "debug" -> buildConstantBase = BuildDevelop()
                "release" -> buildConstantBase = BuildRelease()
            }
        }
    }
}