package com.vinith.imdbapp.core.build

class BuildDevelop : BuildConstantBase() {

    override fun isDevelopBuild(): Boolean = true

    override fun isLogEnabled(): Boolean = true
}