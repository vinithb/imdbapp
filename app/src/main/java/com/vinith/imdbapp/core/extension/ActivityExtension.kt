package com.vinith.imdbapp.core.extension

import androidx.appcompat.app.AppCompatActivity
import com.vinith.imdbapp.core.platform.BaseFragment

fun AppCompatActivity.loadFragment(fragment: BaseFragment, containerId: Int) {
    supportFragmentManager.beginTransaction().add(containerId, fragment)
        .commit()
}

fun AppCompatActivity.replaceFragment(fragment: BaseFragment, containerId: Int) {
    supportFragmentManager.beginTransaction().add(containerId, fragment).addToBackStack(null)
        .commit()
}
