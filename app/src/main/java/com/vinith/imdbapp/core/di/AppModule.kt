package com.vinith.imdbapp.core.di

import android.content.Context
import com.vinith.imdbapp.ImdbApp
import com.vinith.imdbapp.feature.searchTitle.data.MovieRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: ImdbApp) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application

    @Provides
    @Singleton
    fun provideMovieRepository(dataSource: MovieRepository.Network): MovieRepository = dataSource
}