package com.vinith.imdbapp.core.di

import com.vinith.imdbapp.ImdbApp
import com.vinith.imdbapp.core.di.api.ApiModule
import com.vinith.imdbapp.core.di.viewmodel.ViewModelModule
import com.vinith.imdbapp.feature.searchTitle.presentation.MovieDetailFragment
import com.vinith.imdbapp.feature.searchTitle.presentation.MovieSearchFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ApiModule::class, ViewModelModule::class])
interface AppComponent {
    fun inject(application: ImdbApp)
    fun inject(movieSearchFragment: MovieSearchFragment)
    fun inject(movieDetailFragment: MovieDetailFragment)
}