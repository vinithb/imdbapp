package com.vinith.imdbapp.core.api

import com.vinith.imdbapp.feature.searchTitle.model.entity.MovieEntity
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ImdbApi {

    companion object {
        const val HOSTNAME = "https://v2.sg.media-imdb.com/"
    }

    @GET("suggestion/titles/{title}")
    fun searchTitles(@Path("title", encoded = true) title: String?): Call<MovieEntity>
}