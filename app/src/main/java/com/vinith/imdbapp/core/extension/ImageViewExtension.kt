package com.vinith.imdbapp.core.extension

import android.net.Uri
import android.widget.ImageView
import com.squareup.picasso.Picasso

fun ImageView.loadImage(imageUrl: String) {
    Picasso.get()
        .load(Uri.parse(imageUrl))
        .resize(360, 480)
        .onlyScaleDown()
        .into(this)
}