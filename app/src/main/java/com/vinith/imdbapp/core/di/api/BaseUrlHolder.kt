package com.vinith.imdbapp.core.di.api

data class BaseUrlHolder(var baseUrl: String)