package com.vinith.imdbapp.core.build

class BuildRelease: BuildConstantBase() {

    override fun isReleaseBuild(): Boolean = true

    override fun isLogEnabled(): Boolean = false
}