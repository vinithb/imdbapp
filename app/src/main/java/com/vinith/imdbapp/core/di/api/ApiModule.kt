package com.vinith.imdbapp.core.di.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.vinith.imdbapp.core.api.ImdbApi
import com.vinith.imdbapp.core.build.BuildConstant
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module(includes = [OkHttpClientModule::class])
class ApiModule {

    @Singleton
    @Provides
    fun provideImdbApi(@Named("retrofit") retrofit: Retrofit): ImdbApi =
        retrofit.create(ImdbApi::class.java)

    @Singleton
    @Named("retrofit")
    @Provides
    fun provideRetrofit(@Named("okhttpclient")okHttpClient: OkHttpClient,
                        gsonConverterFactory: GsonConverterFactory, baseUrlHolder: BaseUrlHolder): Retrofit =
        Retrofit.Builder().client(okHttpClient)
            .baseUrl(baseUrlHolder.baseUrl)
            .addConverterFactory(gsonConverterFactory)
            .build()

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder().create()
    }

    @Singleton
    @Provides
    fun gsonConverterFactory(gson: Gson): GsonConverterFactory =
        GsonConverterFactory.create(gson)

    @Singleton
    @Provides
    fun provideBaseUrlHolder(): BaseUrlHolder = BaseUrlHolder(BuildConstant.buildConstantBase.getBaseApi())
}
