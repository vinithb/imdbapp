package com.vinith.imdbapp.core.build

import com.vinith.imdbapp.core.api.ImdbApi

abstract class BuildConstantBase {

    open fun getBaseApi(): String  = ImdbApi.HOSTNAME

    open fun isLogEnabled(): Boolean = false

    open fun isDevelopBuild(): Boolean = false

    open fun isReleaseBuild(): Boolean = false

}