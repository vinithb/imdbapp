package com.vinith.imdbapp.core.di.api

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class OkHttpClientModule {

    companion object {
        const val READ_TIMEOUT = 60L
        const val CONNECT_TIMEOUT = 60L
    }

    @Singleton
    @Named("okhttpclient")
    @Provides
    fun okHttpClient(@Named("log_interceptor") httpLoggingInterceptor: HttpLoggingInterceptor):
            OkHttpClient {
        val okHttpClient = OkHttpClient().newBuilder()
        okHttpClient.readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
        okHttpClient.connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
        okHttpClient.addInterceptor(httpLoggingInterceptor)
        return okHttpClient.build()
    }

    @Singleton
    @Named("log_interceptor")
    @Provides
    fun httpLogInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }
}