package com.vinith.imdbapp.core.extension

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.vinith.imdbapp.core.platform.BaseFragment

fun BaseFragment.showToastMsgSmall(msg: String) {
    Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
}

fun BaseFragment.isNetworkAvailable(): Boolean {
    val connectivityManager =
        context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val nw = connectivityManager.activeNetwork ?: return false
    val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
    return when {
        actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
        actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
        else -> false
    }
}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun BaseFragment.registerNetworkCallback(networkAvailable: BaseFragment.NetworkAvailable) {
    val connectivityManager =
        context?.applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkRequest = NetworkRequest.Builder()
        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        .build()
    connectivityManager.registerNetworkCallback(
        networkRequest,
        object : ConnectivityManager.NetworkCallback() {
            override fun onLost(network: Network) {
                networkAvailable.onNetworkAvailable(false)
            }

            override fun onUnavailable() {
                networkAvailable.onNetworkAvailable(false)
            }

            override fun onAvailable(network: Network) {
                networkAvailable.onNetworkAvailable(true)
            }
        })
}