package com.vinith.imdbapp.core.platform

import androidx.fragment.app.Fragment

open class BaseFragment: Fragment() {
    interface NetworkAvailable {
        fun onNetworkAvailable(isAvailable: Boolean)
    }
}